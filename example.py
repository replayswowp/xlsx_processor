import json
from generators.json_data_generator import DataGenerator
from utils.table_to_json import tables_to_html

g_dataGenerator = DataGenerator('test.xlsx')
json_data = g_dataGenerator.generate()

with open('example.html', 'wb') as f:
    f.write((tables_to_html(json_data)).encode('utf-8'))

with open('example.json', 'wb') as f:
    f.write((json.dumps(json_data)).encode('utf-8'))
