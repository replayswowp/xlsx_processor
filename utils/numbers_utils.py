# coding=utf-8
__author__ = "Aleksandr Shyshatsky"

def try_round(variable):
    """
        Если целая часть 4 или больше, то дробную округляем до целых
        Если целая часть 2 или 3  цифры, то оставляем 2 знака после запятой.
        Если целая часть - 0 или 1 знак, то после запятой оставляем 3 знака.
    """
    try:
        if variable >= 1000:
            variable = int(round(variable))

        elif variable >= 10:
            variable = round(variable, 2)

        else:
            variable = round(variable, 3)
        return variable
    except:
        return variable
