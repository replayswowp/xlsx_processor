# coding=utf-8
from __future__ import unicode_literals
from utils.numbers_utils import try_round

__author__ = "Aleksandr Shyshatsky"


def normalize_table(table):
    for row in table:
        for item in range(len(row)):
            row[item] = try_round(row[item])
    return table


def __make_table(table, table_name):
    html = '<p>%s</p><table border="1">' % unicode(table_name)

    for row in table:
        html += '<tr>'
        html += ''.join(map(lambda x: '<td>%s</td>' % ('' if x is None else x), row))  # ('' if x is None else x) поскольку в x может быть None
        html += '</tr>'

    html += '</table>'

    return html


def find_tables(tables):
    """
        Ищет все таблицы в данных из генератора отчета.
    """
    for table_name in tables:
        if type(tables[table_name]) == dict:
            for a, b in find_tables(tables[table_name]):
                yield a, [table_name, b]
        if type(tables[table_name]) == list:
            yield tables[table_name], [table_name]


def tables_to_html(tables):
    html = []
    for table, table_name in find_tables(tables):
        html.append(__make_table(table, table_name))

    return '<meta charset="UTF-8">' + '<br>'.join(html)
