__author__ = "Aleksandr Shyshatsky"


class AutoIncrementVariableGenerator(object):
    """
        Generates numbers from 'start' to 'max_int'.
    """
    __slots__ = 'i'

    def __init__(self, start=0):
        self.i = start - 1

    def __next__(self):
        self.i += 1
        return self.i
