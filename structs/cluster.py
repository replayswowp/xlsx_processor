from utils.magic_variables import AutoIncrementVariableGenerator

__author__ = "Aleksandr Shyshatsky"


class Cluster:
    def __init__(self, row):
        it = AutoIncrementVariableGenerator()
        self.A = row[it.__next__()].value
        self.B = row[it.__next__()].value
        self.C = row[it.__next__()].value
        self.D = row[it.__next__()].value
        self.E = row[it.__next__()].value
        self.F = row[it.__next__()].value
        self.G = row[it.__next__()].value
        self.H = row[it.__next__()].value
        self.I = row[it.__next__()].value
        self.J = row[it.__next__()].value
        self.K = row[it.__next__()].value
        self.L = row[it.__next__()].value
        self.M = row[it.__next__()].value
        self.N = row[it.__next__()].value
        self.O = row[it.__next__()].value
        self.P = row[it.__next__()].value
        self.Q = row[it.__next__()].value
        self.R = row[it.__next__()].value
        self.S = row[it.__next__()].value
        self.T = row[it.__next__()].value
        self.U = row[it.__next__()].value
        self.V = row[it.__next__()].value
        self.W = row[it.__next__()].value
        self.X = row[it.__next__()].value
        self.Y = row[it.__next__()].value
        self.Z = row[it.__next__()].value
        self.AA = row[it.__next__()].value
        self.AB = row[it.__next__()].value
        self.AC = row[it.__next__()].value
        self.AD = row[it.__next__()].value
        self.AE = row[it.__next__()].value
        self.AF = row[it.__next__()].value
        self.AG = row[it.__next__()].value
        self.AH = row[it.__next__()].value
        self.AI = row[it.__next__()].value
        self.AJ = row[it.__next__()].value
        self.AK = row[it.__next__()].value
        self.AL = row[it.__next__()].value
        self.AM = row[it.__next__()].value
        self.AN = row[it.__next__()].value
        self.AO = row[it.__next__()].value
        self.AP = row[it.__next__()].value
        self.AQ = row[it.__next__()].value
        self.AR = row[it.__next__()].value
        self.AS = row[it.__next__()].value
        self.AT = row[it.__next__()].value
        self.AU = row[it.__next__()].value
        self.AV = row[it.__next__()].value
        self.AW = row[it.__next__()].value
        self.AX = row[it.__next__()].value
        self.AY = row[it.__next__()].value
        self.AZ = row[it.__next__()].value
        self.BA = row[it.__next__()].value
        self.BB = row[it.__next__()].value
        self.BC = row[it.__next__()].value
        self.BD = row[it.__next__()].value
        self.BE = row[it.__next__()].value
        self.BF = row[it.__next__()].value
        self.BG = row[it.__next__()].value
        self.BH = row[it.__next__()].value
        del it

    def __str__(self):
        return str(self.__dict__)
