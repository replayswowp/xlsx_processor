from utils.magic_variables import AutoIncrementVariableGenerator

__author__ = "Aleksandr Shyshatsky"

class Pokazatel:
    def __init__(self, block, index):
        self.row_0 = block[0][index].value
        self.row_1 = block[1][index].value
        self.row_2 = block[2][index].value
        self.row_3 = block[3][index].value
        self.row_4 = block[4][index].value
        self.row_5 = block[5][index].value
        self.row_6 = block[6][index].value
        self.row_7 = block[7][index].value
        self.row_8 = block[8][index].value
        self.row_9 = block[9][index].value
        self.row_10 = block[10][index].value

    def __getitem__(self, item):
        return getattr(self, 'row_%s'%item)

    def __str__(self):
        return str(self.__dict__)


class Info:
    def __init__(self, block):
        header = block[0]

        it = AutoIncrementVariableGenerator()
        self.A = header[it.__next__()].value
        self.B = header[it.__next__()].value
        self.C = header[it.__next__()].value
        self.D = header[it.__next__()].value
        self.E = header[it.__next__()].value
        self.F = header[it.__next__()].value

        self.G = Pokazatel(block, 6)
        self.H = Pokazatel(block, 7)
        self.I = Pokazatel(block, 8)
        self.J = Pokazatel(block, 9)
        self.K = Pokazatel(block, 10)
        self.L = Pokazatel(block, 11)
        self.M = Pokazatel(block, 12)
        self.N = Pokazatel(block, 13)
        self.O = Pokazatel(block, 14)
        self.P = Pokazatel(block, 15)
        self.Q = Pokazatel(block, 16)
        self.R = Pokazatel(block, 17)
        self.S = Pokazatel(block, 18)
        self.T = Pokazatel(block, 19)
        self.U = Pokazatel(block, 20)
        self.V = Pokazatel(block, 21)

        del it

    def __str__(self):
        return map(str, self.__dict__.values())