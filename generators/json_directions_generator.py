# coding=utf-8
from __future__ import unicode_literals
__author__ = "Aleksandr Shyshatsky"


class DirectionsGenerator:
    """
        Генерируем табличку "направления", для каждого года - своя.
    """
    def __init__(self, clusters):
        self.clusters = clusters

    def generate(self):

        zones_regions_clusters = {}
        for cluster in self.clusters:
            if not cluster['A'] in zones_regions_clusters: #по годам
                zones_regions_clusters[cluster['A']] = {}

            if not cluster['F'] in zones_regions_clusters[cluster['A']]: #по зонам
                zones_regions_clusters[cluster['A']][cluster['F']] = {}

            if not cluster['D'] in zones_regions_clusters[cluster['A']][cluster['F']]: # по районам
                zones_regions_clusters[cluster['A']][cluster['F']][cluster['D']] = {}

            if not cluster['CW'] in zones_regions_clusters[cluster['A']][cluster['F']][cluster['D']]: # по перегрузкам
                zones_regions_clusters[cluster['A']][cluster['F']][cluster['D']][cluster['CW']] = {}

            if not cluster['CX'] in zones_regions_clusters[cluster['A']][cluster['F']][cluster['D']][cluster['CW']]:  # по сортировкам
                zones_regions_clusters[cluster['A']][cluster['F']][cluster['D']][cluster['CW']][cluster['CX']] = {}

            if not cluster['BS'] in zones_regions_clusters[cluster['A']][cluster['F']][cluster['D']][cluster['CW']][cluster['CX']]:  # по полигонам
                    zones_regions_clusters[cluster['A']][cluster['F']][cluster['D']][cluster['CW']][cluster['CX']][cluster['BS']] = []
            zones_regions_clusters[cluster['A']][cluster['F']][cluster['D']][cluster['CW']][cluster['CX']][cluster['BS']].append(cluster)

        data = {}
        for year in zones_regions_clusters: # группируем по годам, зонам, регионам, перегрузкам, сортировкам и полигонам
            header = ['Зона РО', 'Район', 'Перегрузка', 'Обработка', 'Полигон', 'Образовано отходов', 'Затраты на транспорт 1', 'Затраты на обработку', 'Масса после обработки', 'Затраты на транспорт 2', 'Затраты на обработку на полигоне', 'Масса на захоронение', 'Затраты на захоронение']
            rows = [header, ]
            for zone in zones_regions_clusters[year]:
                for region in zones_regions_clusters[year][zone]:
                    for reload in zones_regions_clusters[year][zone][region]:
                        for sorting in zones_regions_clusters[year][zone][region][reload]:
                            for polygon in zones_regions_clusters[year][zone][region][reload][sorting]:

                                clusters = zones_regions_clusters[year][zone][region][reload][sorting][polygon]
                                row = [zone, region, reload, sorting, polygon,
                                       (sum(map(lambda x: x['AA'], clusters))), # образовано отходов
                                       (sum(map(lambda x: x['CA'], clusters))), # транспорт 1
                                       (sum(map(lambda x: x['BL'], clusters))), # на обработку
                                       (sum(map(lambda x: x['BM'], clusters))), # масса после обраб
                                       (sum(map(lambda x: x['CB'], clusters))),
                                       (sum(map(lambda x: x['BP'] or 0, clusters))),
                                       (sum(map(lambda x: x['BV'], clusters))),
                                       (sum(map(lambda x: x['BU'], clusters)))] # траты на захоронение

                                rows.append(row)

            data[year] = rows
        return data