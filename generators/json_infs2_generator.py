from collections import OrderedDict

__author__ = "Aleksandr Shyshatsky"


class Info2Generator:
    def __init__(self, info):
        self.info = info

    def generate(self):
        result = []

        for info in self.info:
            data = [OrderedDict() for i in range(11)]

            for i in range(11):
                data[i]['A'] = result[i - 1]['A'] if not info.A else info.A
                data[i]['B'] = result[i - 1]['B'] if not info.D else info.D
                data[i]['C'] = result[i - 1]['C'] if not info.B else info.B
                data[i]['D'] = result[i - 1]['D'] if not info.C else info.C
                data[i]['E'] = info.E
                data[i]['F'] = info.F

                data[i]['G'] = info.G[i]
                data[i]['H'] = info.H[i]
                data[i]['I'] = info.I[i]
                data[i]['J'] = info.J[i]
                data[i]['K'] = info.K[i]
                data[i]['L'] = info.L[i]
                data[i]['M'] = info.M[i]
                data[i]['N'] = info.N[i]
                data[i]['O'] = info.O[i]
                data[i]['P'] = info.P[i]
                data[i]['Q'] = info.Q[i]
                data[i]['R'] = info.R[i]
                data[i]['S'] = info.S[i]
                data[i]['T'] = info.T[i]
                data[i]['U'] = info.U[i]
                data[i]['V'] = info.V[i]

            result += data

        return result