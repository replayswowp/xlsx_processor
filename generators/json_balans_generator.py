# coding=utf-8
from __future__ import unicode_literals
__author__ = "Aleksandr Shyshatsky"


class BalansGenerator:
    """
        Генерирует инфу о балансе (для каждой зоны и общую)
    """
    def __init__(self, clusters2, infs2):
        self.clusters2 = clusters2
        self.infs2 = infs2

    def generate(self):
        self.clusters_by_zones = []
        self.infs_by_zones = []

        available_zones = set(map(lambda x: x['F'], self.clusters2)) # находим все возможные зоны в кластерах
        for zone_name in available_zones:
            zone = filter(lambda x: x['F'] == zone_name, self.clusters2)
            infs_zone = filter(lambda x: x['B'] == zone_name, self.infs2)

            if len(zone) == 0 or len(infs_zone) == 0:
                break
            self.clusters_by_zones.append(zone)
            self.infs_by_zones.append(infs_zone)

        self.zones_info = []
        result = {}
        for i in range(len(self.clusters_by_zones)): # обрабатываем данные по кластерам в каждой зоне
            self.zones_info.append(self.__process_zone(self.clusters_by_zones[i], i + 1, self.infs_by_zones[i]))

            result[self.zones_info[-1][0][0]] = self.zones_info[-1]  # result['имя_зоны'][первая строка][первая ячейка] = 'Зона - X'

        result['total'] = self.__process_total() # суммарная инфа о всех зонах
        return result

    def __safe_div(self, a, b, default=0):
        return a / b if b != 0 else (None if a != 0 else default)

    def __process_total(self):
        """
            Считаем суммарную инфу по всем зонам.
        """
        data = []
        header = ['Всего по Ярославской области', 'Ед. изм.', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024',
                  '2025', '2026', '2027', '2028', '2029', '2030']
        data.append(header)

        total_cost = 'тыс. руб.'
        total_cost_r = 'руб. / тонна'
        total_cost_p = '%'
        total_cost_t = 'тыс. тонн'

        row1 = [u'Расходы регионального оператора всего', total_cost]
        row2 = [u'Всего на транспортирование (1 плечо)', total_cost]
        row3 = [u'Всего на транспортирование (2 плечо)', total_cost]
        row4 = [u'Всего на перегрузку', total_cost]
        row5 = [u'Всего на обработку', total_cost]
        row6 = [u'Всего на обезвреживание отходов', total_cost]
        row7 = [u'Всего на размещение', total_cost]

        row8 = [u'Всего доп. расходы регоператора', total_cost]
        row9 = [u'Единый тариф регионального оператота', total_cost_r]
        row10 = [u'С учетом 18% НДС', total_cost_r]

        row11 = [u'Расходы на транспортирование (1 плечо)', total_cost_r]
        row12 = [u'Расходы на транспортирование (2 плечо)', total_cost_r]
        row13 = [u'Расходы на перегрузку', total_cost_r]
        row14 = [u'Расходы на обработку отходов', total_cost_r]
        row15 = [u'Расходы на обезврежинваие отходов', total_cost_r]
        row16 = [u'Расходы на захоронение отходов', total_cost_r]
        row17 = [u'Дополнительные расходы рег. оператора', total_cost]
        row18 = [u'Средний тариф на обработку отходов', total_cost]
        row19 = [u'Средний тариф на перегрузку', total_cost]
        row20 = [u'Средний тариф на захоронение отходов', total_cost]

        row21 = [u'Образовано отходов', total_cost_t]
        row22 = [u'Поступило из других субъектов', total_cost_t]
        row23 = [u'Обработано отходов', total_cost_t]
        row24 = [u'Прошло перегрузку/прессовку', total_cost_t]
        row25 = [u'Обезврежено отходов', total_cost_t]
        row26 = [u'Утилизировано отходов', total_cost_t]
        row27 = [u'Размещено отходов', total_cost_t]
        row28 = [u'Передано в другие субъекты', total_cost_t]

        row29 = [u'Доля обработанных отходов', total_cost_p]
        row30 = [u'Доля перегруженных отходов', total_cost_p]
        row31 = [u'Доля обезвреженных отходов', total_cost_p]
        row32 = [u'Доля утилизированных отходов', total_cost_p]
        row33 = [u'Доля размещенных отходов', total_cost_p]

        for year in range(2016, 2031):
            index = year - 2016 + 2
            row1.append(sum(map(lambda x: x[1][index], self.zones_info)))
            row2.append(sum(map(lambda x: x[2][index], self.zones_info)))
            row3.append(sum(map(lambda x: x[3][index], self.zones_info)))
            row4.append(sum(map(lambda x: x[4][index], self.zones_info)))
            row5.append(sum(map(lambda x: x[5][index], self.zones_info)))
            row6.append(sum(map(lambda x: x[6][index], self.zones_info)))
            row7.append(sum(map(lambda x: x[7][index], self.zones_info)))
            row8.append(sum(map(lambda x: x[8][index], self.zones_info)))

            row21.append(sum(map(lambda x: x[21][index], self.zones_info)))
            row22.append(sum(map(lambda x: x[22][index], self.zones_info)))
            row23.append(sum(map(lambda x: x[23][index], self.zones_info)))
            row24.append(sum(map(lambda x: x[24][index], self.zones_info)))
            row25.append(sum(map(lambda x: x[25][index], self.zones_info)))
            row26.append(sum(map(lambda x: x[26][index], self.zones_info)))
            row27.append(sum(map(lambda x: x[27][index], self.zones_info)))
            row28.append(sum(map(lambda x: x[28][index], self.zones_info)))

            row18.append(self.__safe_div(row5[-1], row23[-1]))

            row11.append(row2[-1] / (row21[-1] or 1))
            row12.append(row3[-1] / (row21[-1] or 1))
            row13.append(row4[-1] / (row21[-1] or 1))
            row14.append(row5[-1] / (row21[-1] or 1))
            row15.append(row6[-1] / (row21[-1] or 1))
            row16.append(row7[-1] / (row21[-1] or 1))
            row17.append(row8[-1] / (row21[-1] or 1))

            row29.append(round(100 * row23[-1] / ((row21[-1] + row22[-1]) or 1), 1))
            row30.append(round(100 * row24[-1] / ((row21[-1] + row22[-1]) or 1), 1))
            row31.append(round(100 * row25[-1] / ((row21[-1] + row22[-1]) or 1), 1))
            row32.append(round(100 * row26[-1] / ((row21[-1] + row22[-1]) or 1), 1))
            row33.append(round(100 * row27[-1] / ((row21[-1] + row22[-1]) or 1), 1))

            row19.append(self.__safe_div(row4[-1], row24[-1]))

            row20.append(self.__safe_div(row7[-1], row27[-1]))
            row9.append(row1[-1] / (row21[-1] or 1) + row17[-1])
            row10.append(row9[-1] * 1.18)

        data.append(row1)
        data.append(row2)
        data.append(row3)
        data.append(row4)
        data.append(row5)
        data.append(row6)
        data.append(row7)
        data.append(row8)
        data.append(row9)
        data.append(row10)
        data.append(row11)
        data.append(row12)
        data.append(row13)
        data.append(row14)
        data.append(row15)
        data.append(row16)
        data.append(row17)
        data.append(row18)
        data.append(row19)
        data.append(row20)
        data.append(row21)
        data.append(row22)
        data.append(row23)
        data.append(row24)
        data.append(row25)
        data.append(row26)
        data.append(row27)
        data.append(row28)
        data.append(row29)
        data.append(row30)
        data.append(row31)
        data.append(row32)
        data.append(row33)

        return data

    def __process_zone(self, zone, num, infs_zone):
        """
            Обработка зоны. Формулы взяты из excel'я.
        """
        data = []
        header = [zone[0]['F'], 'Ед. изм.', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030']
        data.append(header)

        total_cost = 'тыс. руб.'
        total_cost_r = 'руб. / тонна'
        total_cost_p = '%'
        total_cost_t = 'тыс. тонн'

        row1 = [u'Расходы регионального оператора всего', total_cost]
        row2 = [u'Всего на транспортирование (1 плечо)', total_cost]
        row3 = [u'Всего на транспортирование (2 плечо)', total_cost]
        row4 = [u'Всего на перегрузку', total_cost]
        row5 = [u'Всего на обработку', total_cost]
        row6 = [u'Всего на обезвреживание отходов', total_cost]
        row7 = [u'Всего на размещение', total_cost]

        row8 = [u'Всего доп. расходы регоператора', total_cost]
        row9 = [u'Единый тариф регионального оператота', total_cost_r]
        row10 = [u'С учетом 18% НДС', total_cost_r]

        row11 = [u'Расходы на транспортирование (1 плечо)', total_cost_r]
        row12 = [u'Расходы на транспортирование (2 плечо)', total_cost_r]
        row13 = [u'Расходы на перегрузку', total_cost_r]
        row14 = [u'Расходы на обработку отходов', total_cost_r]
        row15 = [u'Расходы на обезврежинваие отходов', total_cost_r]
        row16 = [u'Расходы на захоронение отходов', total_cost_r]
        row17 = [u'Дополнительные расходы рег. оператора', total_cost]
        row18 = [u'Средний тариф на обработку отходов', total_cost]
        row19 = [u'Средний тариф на перегрузку', total_cost]
        row20 = [u'Средний тариф на захоронение отходов', total_cost]

        row21 = [u'Образовано отходов', total_cost_t]
        row22 = [u'Поступило из других субъектов', total_cost_t]
        row23 = [u'Обработано отходов', total_cost_t]
        row24 = [u'Прошло перегрузку/прессовку', total_cost_t]
        row25 = [u'Обезврежено отходов', total_cost_t]
        row26 = [u'Утилизировано отходов', total_cost_t]
        row27 = [u'Размещено отходов', total_cost_t]
        row28 = [u'Передано в другие субъекты', total_cost_t]

        row29 = [u'Доля обработанных отходов', total_cost_p]
        row30 = [u'Доля перегруженных отходов', total_cost_p]
        row31 = [u'Доля обезвреженных отходов', total_cost_p]
        row32 = [u'Доля утилизированных отходов', total_cost_p]
        row33 = [u'Доля размещенных отходов', total_cost_p]

        for year in range(2016, 2031):
            year_zone = filter(lambda x: x['A'] == year, zone)
            disposal_zone = filter(lambda x: x['J'] == 'disposal', year_zone)
            infs_filtered_ass = filter(lambda x: (x['C'] == 'assortment') and (x['G'] == u'Из других регионов'), infs_zone)
            infs_filtered_disp = filter(lambda x: (x['C'] == 'disposal') and (x['G'] == u'Из других регионов'), infs_zone)
            infs_filtered_place = filter(lambda x: (x['C'] == 'placement') and (x['G'] == u'Из других регионов'), infs_zone)

            row1.append((sum(map(lambda x: x['AY'], year_zone))))
            row2.append((sum(map(lambda x: x['AP'], year_zone))))
            row3.append((sum(map(lambda x: x['AQ'], year_zone))))
            row4.append((sum(map(lambda x: x['CE'], year_zone))))
            row5.append((sum(map(lambda x: x['CF'], year_zone))))
            row6.append((sum(map(lambda x: x['AT'], year_zone))))
            row7.append((sum(map(lambda x: x['AU'], year_zone))))

            row21.append((sum(map(lambda x: x['AA'], year_zone))))

            row23.append((sum(map(lambda x: x['CH'], year_zone))) + sum(map(lambda x: x['H'] or 0, infs_filtered_ass)))
            row24.append((sum(map(lambda x: x['CG'], year_zone))))
            row25.append((sum(map(lambda x: x['AF'], year_zone))) + sum(map(lambda x: x['H'] or 0, infs_filtered_disp)))
            row26.append((sum(map(lambda x: x['CI'], year_zone))))
            row27.append((sum(map(lambda x: x['AG'], year_zone))) + sum(map(lambda x: x['H'] or 0, infs_filtered_place)))
            row28.append(0)

            row22.append(abs(round(row27[-1] - row21[-1] + row26[-1])))

            row29.append(round(100 * row23[-1] / ((row21[-1] + row22[-1]) or 1), 1))
            row30.append(round(100 * row24[-1] / ((row21[-1] + row22[-1]) or 1), 1))
            row31.append(round(100 * row25[-1] / ((row21[-1] + row22[-1]) or 1), 1))
            row32.append(round(100 * row26[-1] / ((row21[-1] + row22[-1]) or 1), 1))
            row33.append(round(100 * row27[-1] / ((row21[-1] + row22[-1]) or 1), 1))

            row8.append((row1[year - 2016 + 2] * 0.1))

            row17.append((row8[year - 2016 + 2] / (row21[year - 2016 + 2] or 1)))

            row9.append((1.0 * row1[year - 2016 + 2] / (row21[year - 2016 + 2] or 1)) + row17[year - 2016 + 2])
            row10.append(row9[-1] * 1.18)

            row11.append(row2[-1] / (row21[-1] or 1))
            row12.append(row3[-1] / (row21[-1] or 1))

            row13.append(row4[-1] / (row21[-1] or 1))
            row14.append(row5[-1] / (row21[-1] or 1))
            row15.append(row6[-1] / (row21[-1] or 1))
            row16.append(row7[-1] / (row21[-1] or 1))

            row18.append((row5[-1] / row23[-1]) if row23[-1] > 0 else None)
            row19.append((row4[-1] / row24[-1]) if row24[-1] > 0 else None)
            row20.append((row7[-1] / row27[-1]) if row27[-1] > 0 else None)

        data.append(row1)
        data.append(row2)
        data.append(row3)
        data.append(row4)
        data.append(row5)
        data.append(row6)
        data.append(row7)

        data.append(row8)
        data.append(row9)
        data.append(row10)

        data.append(row11)
        data.append(row12)
        data.append(row13)
        data.append(row14)
        data.append(row15)
        data.append(row16)
        data.append(row17)
        data.append(row18)
        data.append(row19)
        data.append(row20)

        data.append(row21)
        data.append(row22)
        data.append(row23)
        data.append(row24)
        data.append(row25)
        data.append(row26)
        data.append(row27)
        data.append(row28)
        data.append(row29)
        data.append(row30)
        data.append(row31)
        data.append(row32)
        data.append(row33)

        return data
