# coding=utf-8
from math import ceil

__author__ = "Aleksandr Shyshatsky"


class Clusters2Generator:
    """
        Генерирует данные из таблички clusters2, которые нужны для дальнейших вычислений.
    """
    def __init__(self, clusters):
        self.clusters = clusters

    def generate(self):
        data = []
        zones = {}
        for cluster in self.clusters:
            if cluster.F in zones:
                zones[cluster.F] += 1
            else:
                zones[cluster.F] = 1

            info = cluster.__dict__.copy()
            info['AE'] = cluster.AE + cluster.AF
            info['AF'] = cluster.AG
            info['AG'] = cluster.AH
            info['AH'] = cluster.AI
            info['AI'] = cluster.AJ
            info['AJ'] = cluster.AK
            info['AK'] = cluster.AL
            info['AL'] = cluster.AM
            info['AM'] = cluster.AN
            info['AN'] = cluster.AO
            info['AO'] = cluster.AP
            info['AP'] = cluster.AQ
            info['AQ'] = cluster.AR
            info['AR'] = cluster.AS

            info['AS'] = cluster.AT + cluster.AU

            info['AT'] = cluster.AV
            info['AU'] = cluster.AW
            info['AV'] = cluster.AX
            info['AW'] = cluster.AY
            info['AX'] = cluster.AZ
            info['AY'] = cluster.BA
            info['AZ'] = cluster.BB
            info['BA'] = cluster.BC
            info['BB'] = cluster.BD
            info['BC'] = cluster.BF
            info['BD'] = cluster.BG
            info['BE'] = cluster.BH

            info['BF'] = (cluster.AA * (1 - cluster.X)) if cluster.AZ > 0 else None
            info['BG'] = cluster.A
            info['BH'] = cluster.F
            info['BI'] = cluster.L if cluster.G != 'placement' else '-' # обработка 1
            info['BJ'] = cluster.K if info['BI'] != '-' else None  # зона обработка 1
            info['BK'] = cluster.AA if info['BJ'] is not None else None  # масса на обработку 1
            info['BL'] = cluster.AB * cluster.S if info['BK'] is not None else None  # затраты на обработку 1
            info['BM'] = info['BK'] * (1 - cluster.X) if info['BK'] is not None else None  # масса после обработку 1

            info['BS'] = ''.join([(cluster.L if cluster.J == 'placement' else ''), (cluster.P if cluster.N == 'placement' else '')])  # Полигон
            info['BT'] = (cluster.K if cluster.J == 'placement' else cluster.O) if info['BS'] else None  # зона полигона

            info['BN'] = cluster.R if (cluster.I != cluster.Q) and cluster.Q != 'None' and (cluster.Q or 1) > 0 else '-'  # обработка 1
            info['BO'] = info['BT'] if info['BN'] != '-' else None  # зона обработки 2
            info['BP'] = (info['BF'] or 0) * (cluster.W or 0) if info['BO'] else None # затраты обработка 2
            info['BQ'] = info['BF'] if info['BO'] else None # масса на обработку 2
            info['BR'] = (info['BF'] or 0) * (1 - cluster.Y) if info['BO'] else None  # масса после обработку 2
            
            info['BU'] = info['AU']
            info['BV'] = info['AG']

            info['BW'] = info['BS'] # МСЗ
            info['BX'] = ''.join([cluster.K if cluster.J == 'placement' else '', cluster.O if cluster.N == 'placement' else '']) # МСЗ
            info['BY'] = (cluster.AB if cluster.J == 'placement' else 0) + (cluster.AC if cluster.N == 'placement' else 0) #масса на МСЗ
            info['BZ'] = cluster.AV if info['BW'] else None

            info['CA'] = cluster.AQ
            info['CB'] = cluster.AR

            info['CC'] = info['BS'] if not info['BS'] else info['BW'] #конечный пункт
            info['CD'] = info['BX'] if not info['BT'] else info['BT'] #зона конечного пункта

            info['CE'] = cluster.AB * cluster.S if cluster.J == 'transfer' else 0  #затраты перегрузка
            info['CF'] = (cluster.AB * cluster.S if cluster.J == 'assortment' else 0) + cluster.AZ #затраты сортировка

            info['CG'] = cluster.AB if cluster.J == 'transfer' else 0 #масса перегрузки
            info['CH'] = cluster.AB if cluster.J == 'assortment' or cluster.Q else 0 #масса сортированных

            info['CI'] = cluster.AA - cluster.AH
            
            info['CJ'] = None

            info['CK'] = info['BY'] if not cluster.AW else cluster.AW

            info['CL'] = cluster.AH or info['BZ'] #FIXME

            info['CM'] = None
            info['CN'] = None
            info['CO'] = None
            info['CP'] = None
            info['CQ'] = 5
            info['CR'] = None

            info['CS'] = int(ceil((cluster.AA * 1000 / 365.0) / 0.091)) # количество контейнеров
            info['CT'] = None
            info['CU'] = None

            info['CV'] = int(ceil(cluster.AA / 0.1001))

            info['CW'] = cluster.L if cluster.J == 'transfer' else '-'

            info['CX'] = cluster.L if cluster.J == 'assortment' else (cluster.R or '-')

            if len(data):
                info['CY'] = data[-1]['CY'] + 1 if zones.get(cluster.F, 0) == 1 else data[-1]['CY']
                info['CZ'] = info['CY'] if (info['CY'] != data[-1]['CY']) and cluster.A else None
                info['DA'] = cluster.F if (info['CY'] != data[-1]['CY']) else None
            else:
                info['CY'] = 1
                info['CZ'] = 1
                info['DA'] = cluster.F

            data.append(info)

        return data
