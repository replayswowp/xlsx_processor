# coding=utf-8
__author__ = "Aleksandr Shyshatsky"


class TarifGenerator:
    """
        Генератор таблицы "Тарифы". Работает совместно с таблицей "расчет"
    """
    def __init__(self):
        self.zones = {}

    def generate(self):
        data = []
        header = [u'Единый тариф регионального оператора'] + [i for i in range(2016, 2031)]
        data.append(header)
        for name in self.zones:
            row = [name] + [self.zones[name][i] for i in range(2016, 2031)]
            data.append(row)
        return data

    def addZoneInfo(self, zone_name, total_nds, year):
        if zone_name not in self.zones:
            self.zones[zone_name] = {}
        self.zones[zone_name][year] = total_nds

g_tariffs = TarifGenerator()
