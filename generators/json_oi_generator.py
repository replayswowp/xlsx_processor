# coding=utf-8
from __future__ import unicode_literals
from collections import OrderedDict

__author__ = "Aleksandr Shyshatsky"


class OIGenerator:
    """
        Генератор таблицы ОИ.
    """
    def __init__(self, infs2):
        self.infs2 = infs2

    def generate(self):
        table = []
        pure_row = OrderedDict({
            'A': '',
            'B': '',
            'C': '',
            'D': '',
            'E': '',
            'F': '',
            'G': '',
            'H': '',
            'I': '',
            'J': '',
            'K': '',
            'L': '',
            'M': '',
            'N': '',
            'O': '',
            'P': '',
            'Q': '',
            'R': '',
            'S': '',
            'T': '',
            'U': '',
            'V': '',
            'W': '',
            'X': '',
            'Y': '',
            'Z': '',
        })

        header = [
            'Зона РО',
            'Наименование',
            'Показатель',
            '2016',
            '2017',
            '2018',
            '2019',
            '2020',
            '2021',
            '2022',
            '2023',
            '2024',
            '2025',
            '2026',
            '2027',
            '2028',
            '2029',
            '2030',
            '',
            '',
            '',
            '',
            'Тип объекта',
            'Поля полигонов',
            'Поля обработки',
            'Вывод',
        ]

        table.append(header)
        for i in xrange(0, len(self.infs2), 11):
            table.append(pure_row.copy().values())

            def get_row(k):
                oth = []
                oth.append(self.infs2[i + k]['B'])
                oth.append(self.infs2[i + k]['D'] if self.infs2[i + k]['G'] else None)
                oth.append(self.infs2[i + k]['G'])

                oth.append(self.infs2[i + k]['H'])
                oth.append(self.infs2[i + k]['I'])
                oth.append(self.infs2[i + k]['J'])
                oth.append(self.infs2[i + k]['K'])
                oth.append(self.infs2[i + k]['L'])
                oth.append(self.infs2[i + k]['M'])
                oth.append(self.infs2[i + k]['N'])
                oth.append(self.infs2[i + k]['O'])
                oth.append(self.infs2[i + k]['P'])
                oth.append(self.infs2[i + k]['Q'])
                oth.append(self.infs2[i + k]['R'])
                oth.append(self.infs2[i + k]['S'])
                oth.append(self.infs2[i + k]['T'])
                oth.append(self.infs2[i + k]['U'])
                oth.append(self.infs2[i + k]['V'])

                oth.append(None)
                oth.append(None)
                oth.append(None)
                oth.append(None)

                oth.append(self.infs2[i + k]['C'])
                oth.append((1 if oth[2] in [u'Емкость на начало года', u'Мощность', u'Завезено отходов', u'Емкость на конец года', u'Тариф'] else 0) if oth[-1] == 'placement' else 0)
                oth.append((1 if (oth[2] in [u'Мощность', u'Завезено отходов', u'Тариф']) else 0) if oth[-2] != 'placement' else 0)
                oth.append((1 if (not oth[2] or oth[-2] > 0 or oth[-2] > 0) else 0) if oth[-3] != 'disposal' else 0)

                return oth

            oth = get_row(1)
            table.append(oth)

            oth = get_row(2)
            table.append(oth)

            oth = get_row(3)
            table.append(oth)

            oth = get_row(4)
            table.append(oth)

            oth = get_row(5)
            table.append(oth)

            oth = get_row(6)
            table.append(oth)

            oth = get_row(7)
            table.append(oth)

            oth = get_row(8)
            table.append(oth)

            oth = get_row(9)
            table.append(oth)

            oth = get_row(10)
            table.append(oth)

        return table
