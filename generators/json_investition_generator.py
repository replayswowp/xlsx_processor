# coding=utf-8
from __future__ import unicode_literals
__author__ = "Aleksandr Shyshatsky"


class InvestitionsGenerator:
    """
        Таблица "инвестиции"
    """
    def __init__(self, infs2):
        self.infs2 = infs2

    def generate(self):
        self.zones = []
        self.total = ['', ''] + [0 for i in range(2016, 2031)]
        available_zones = set(map(lambda x: x['B'], self.infs2))  # находим все возможные зоны в кластерах
        for zone_name in available_zones:
            infs_zone = filter(lambda x: x['B'] == zone_name, self.infs2)

            if len(infs_zone) == 0:
                break
            self.zones.append(infs_zone)

        investitions_zones = [['Зона РО', 'Вид объектов', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030']]
        for j in map(self.__create_zone, self.zones):
            investitions_zones += j
        investitions_zones += [self.total]

        return investitions_zones

    def __create_zone(self, zone):
        i = 0
        row1 = [zone[i]['B'], u'Объекты размещения']
        row2 = [zone[i]['B'], u'Объекты сортировки']
        row3 = [zone[i]['B'], u'Перегрузочные станции']
        row4 = [zone[i]['B'], u'Всего']
        row5 = ['', '']

        for year in range(2016, 2031):
            filter_row1 = filter(lambda x: (x['C'] == 'placement') and (x['G'] == u'Капитальные вложения всего'), zone)
            filter_row2 = filter(lambda x: (x['C'] == 'assortment') and (x['G'] == u'Капитальные вложения всего'), zone)
            filter_row3 = filter(lambda x: (x['C'] == 'transfer') and (x['G'] == u'Капитальные вложения всего'), zone)

            row1.append(round(sum(map(lambda x: x[chr(ord('H') + year - 2016)] or 0, filter_row1))))
            row2.append(round(sum(map(lambda x: x[chr(ord('H') + year - 2016)] or 0, filter_row2))))
            row3.append(round(sum(map(lambda x: x[chr(ord('H') + year - 2016)] or 0, filter_row3))))

            row4.append(round(row1[-1] + row2[-1] + row3[-1]))

            self.total[year - 2016 + 2] += (row4[year - 2016 + 2])

        return [row1, row2, row3, row4]

