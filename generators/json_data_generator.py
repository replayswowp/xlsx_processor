# coding=utf-8
"""
    Lasciate ogni speranza, voi ch'entrate
"""
from __future__ import unicode_literals
from json_balans_generator import BalansGenerator
from json_calculation_generator import Calculation2Generator
from json_clusters2_generator import Clusters2Generator
from json_directions_generator import DirectionsGenerator
from json_infs2_generator import Info2Generator
from json_investition_generator import InvestitionsGenerator
from json_oi_generator import OIGenerator
from json_tarifs_generator import g_tariffs
from structs.info import Info
from utils.table_to_json import normalize_table, find_tables

from openpyxl import load_workbook
from structs.cluster import Cluster

__author__ = "Aleksandr Shyshatsky"


class DataGenerator:
    """
        Генератор общего отчета.
    """
    def __init__(self, table_path):
        self.result = {
            'oi': None,
            'investitions': None,
            'calculation': None,
            'balance': None,
            'tariffs': None,
            'directions': None,
        }

        self.wb = load_workbook(table_path, True, True, False, False, True)

    def generate(self):
        """
            Генерирует инфу по каждому листу и собирает их в один большой json.
        """
        infs_sheet = self.wb.get_sheet_by_name(name='infs')
        infs_data = []
        block = []
        i = 0
        for row in infs_sheet.rows:
            if i == 0:
                i += 1
                continue # пропускаем заголовок

            block.append(row)
            if len(block) == 11:
                info = Info(block)
                infs_data.append(info)
                block = []

        infs2_data = Info2Generator(infs_data).generate()

        clusters = self.wb.get_sheet_by_name(name='clusters')
        clusterss = []
        i = 0
        for row in clusters.rows:
            if i == 0:
                i += 1
                continue  # пропускаем заголовок

            cluster = Cluster(row)
            clusterss.append(cluster)

        clusters2_data = Clusters2Generator(clusterss).generate()
        calculations = Calculation2Generator(clusters2_data, infs2_data).generate()
        balans = BalansGenerator(clusters2_data, infs2_data).generate()
        oi = OIGenerator(infs2_data).generate()
        investitions = InvestitionsGenerator(infs2_data).generate()
        tariffs = g_tariffs.generate()
        directions = DirectionsGenerator(clusters2_data).generate()

        self.result['balance'] = balans
        self.result['oi'] = oi
        self.result['investitions'] = investitions
        self.result['tariffs'] = tariffs
        self.result['directions'] = directions
        self.result['calculation'] = calculations

        for table, table_name in find_tables(self.result):
            table = normalize_table(table) # округление всего что можно округлить, дабы привести данные в нормальный вид

        return self.result
